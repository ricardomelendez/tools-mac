#!/bin/bash
clear

#Definiendo variables globales
destino="https://prod-10.northcentralus.logic.azure.com:443/workflows/71a462e651774612b6270320abaa8c18/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=YCOpC8pkCKZvgj0WKcWyZuROP-4ro8Hdd-0q4BhrJn4"
infor="EBB06FC4-2F24-407D-B929-ED5AB104A6B7"
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m'

#Obteniendo datos para reportar a casa
serial=$(system_profiler SPHardwareDataType | grep -i "Serial")
IFS=':' read -ra my_array <<< "$serial"
numserial=$(echo ${my_array[1]})

modelo=$(system_profiler SPHardwareDataType | grep -i "Model Name")
IFS=':' read -ra my_arraymodel <<< "$modelo"
typemodelo=$(echo ${my_arraymodel[1]})


machinename=$HOSTNAME
echo "==========================================================="
echo -e "                          ${YELLOW}La Haus${NC}"
echo -e "                 ${YELLOW}Instalacion de Agentes${NC}"
echo -e "               ${RED}ATTENTION : Apple Mac Version${NC}"
echo "==========================================================="

echo "Ingrese email del H@user a cargo de la maquina"
read -p "Usuario:" usuario



#Call 2 Home
echo "==========================================================="
echo -e "${YELLOW}Llamando a casa....${NC}"
#data='"[{"Employee":"'"$usuario"'","Serial":"'"${numserial%%*( )}"'","MachineName":"'"$machinename"'","Brand":"Apple","Model":"'"${typemodelo%%*( )}"'","Token":"'"$infor"'"}]"'
data='{"Employee":"'"$usuario"'","Serial":"'"${numserial%%*( )}"'","MachineName":"'"$machinename"'","Brand":"Apple","Model":"'"${typemodelo%%*( )}"'","Token":"'"$infor"'"}'
echo $data > salida.json
#curl -d $data -H 'Content-Type: application/json' $destino

#Remitiendo datos a casa
curl -X POST --data @salida.json -H "Content-Type: application/json" $destino

if [ $? -eq 0 ]
then
  echo -e "${GREEN}Machine $numserial assigned to $usuario ${NC}"
else
  echo -e "${RED}Call to Home failed.... ${NC}"
fi


#Ruta de almacenamiento
echo "==========================================================="
#cd /tmp
echo $PWD
echo -e "${YELLOW}Descargando archivos requeridos para instalacion...${NC}"
curl "https://assets-it.lahaus.com/mac/Ivanti.Neurons.Agent.dmg" -o "Ivanti.Neurons.Agent.dmg"
curl "https://assets-it.lahaus.com/mac/Sentinel-Release-23-1-2-6782_macos_v23_1_2_6782.pkg" -o "Sentinel-Release-23-1-2-6782_macos_v23_1_2_6782.pkg"
curl "https://assets-it.lahaus.com/mac/Token_Sentinel.txt" -o "com.sentinelone.registration-token"
curl "https://assets-it.lahaus.com/mac/TeamViewerHost.dmg" -o "TeamViewerHost.dmg"

#Instalando IvantiAgent
clear
echo "==========================================================="
echo -e "${YELLOW}Instalando Ivanti Agent.....${NC}"
/usr/bin/hdiutil attach "./Ivanti.Neurons.Agent.dmg" -mountpoint "/tmp/cloudinstall" -nobrowse
installer -pkg "/tmp/cloudinstall/Ivanti Neurons Agent.pkg" -target /
/usr/local/com.ivanti.cloud.agent/IvantiAgent/bin/stagentctl register --baseurl https://agentreg.ivanticloud.com --enrollmentkey 056b9c35-170e-42e7-b50a-677c8c56fd13_2CHhtTb9zSlCMpNPoulBMso8j4DAEERgKMlwY3JfHRiyKMEZLSUsZ2H00kGx41mmoPPzFpBKabof9pzyR7MCAqhGSJr2VhXKVBC1cB4IMOylYDIvLSpeSF8r7QgHULA3
/usr/bin/hdiutil detach "/tmp/cloudinstall"

#Instalando SentinelOne
clear
echo "==========================================================="
echo -e "${YELLOW}Instalando SentinelOne Agent.....${NC}"
chown root com.sentinelone.registration-token
installer -pkg $PWD/Sentinel-Release-23-1-2-6782_macos_v23_1_2_6782.pkg -target /Library/


#Instalando TeamViewer
clear
echo "==========================================================="
echo -e "${YELLOW}Instalando TeamViewer.....${NC}"
installer -pkg /TeamViewerHost.dmg -target

echo "==========================================================="
echo -e "${YELLOW}Eliminando archivos de instalacion...${NC}"
rm Ivanti.Neurons.Agent.dmg
rm com.sentinelone.registration-token
rm Sentinel-Release-23-1-2-6782_macos_v23_1_2_6782.pkg
rm salida.json

echo -e "${GREEN}Finalizado...... ${NC}"
